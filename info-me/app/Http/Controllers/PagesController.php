<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(){
        $title = 'Welcome to Laravel!';
       // return view('Pages.index', compact('title'));
        return view('Pages.index')->with('title', $title);

    }

    public function about(){
        $title = 'ABOUT US';
        return view('Pages.about')->with('title', $title);
    }

    public function services(){
        $data = array (
            'title' => 'SERVICES',
            'services' => ['Web Designer', 'Web Developer', 'Android Developer', 'iOS Developer']
        );
        return view('Pages.services')->with($data);
    }
}
