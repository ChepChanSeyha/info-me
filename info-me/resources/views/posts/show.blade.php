@extends('Layouts.app')

@section('content')

    <a href="/posts" class="btn btn-default">Go Back</a>
    <h1>{{$post->title}}</h1>
    <div>
        {{$post->body}}
    </div>
    <hr>
    <small>Writed on {{$post->created_at}}</small>
    <hr>


{{--    //buton delete post--}}
{{--    <form action="{{ route('posts.destroy', $post->id)}}" method="post">--}}
{{--        @csrf--}}
{{--        @method('DELETE')--}}
{{--        <button class="btn btn-danger" type="submit">Delete</button>--}}
{{--    </form>--}}


{{--    //button update post--}}
{{--    <a href="{{ route('posts.edit',$post->id)}}" class="btn btn-primary">UPDATE</a>--}}
@endsection
