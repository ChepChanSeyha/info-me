@extends('Layouts.app')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <a href="./posts/create" class="btn btn-primary">CREATE</a><hr>
    {{$posts->links()}}

    <div class="uper">
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div><br />
        @endif
        <table class="table table-striped">
            <thead>
            <tr>
                <td>ID</td>
                <td>Title</td>
                <td>Body</td>

                <td colspan="3">Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($posts as $post)
                <tr>
                    <td>{{$post->id}}</td>
                    <td>{{$post->title}}</td>
                    <td>{{$post->body}}</td>

                    <td><a href="/posts/{{$post->id}}" class="btn btn-primary">VIEW</a></td>
                    <td><a href="{{ route('posts.edit',$post->id)}}" class="btn btn-primary">Edit</a></td>
                    <td>
                        <form action="{{ route('posts.destroy', $post->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
        <div>
@endsection
