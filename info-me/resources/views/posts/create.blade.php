@extends('Layouts.app')

@section('content')
    <h1>Create Post</h1>
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="card uper">
        <div class="card-header">
            Add Post
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{ route('posts.store') }}">
                <div class="form-group">
                    @csrf
                    <label for="name">Title:</label>
                    <input type="text" class="form-control" name="title"/>
                </div>
                <div class="form-group">
                    <label for="body">Body:</label>
                    <input type="text" class="form-control" name="body"/>
                </div>
                <button type="submit" class="btn btn-primary">Create</button>

                <a class="btn btn-primary" href="{{ route('posts.index')}}">CANCEL</a>
            </form>
        </div>
    </div>
@endsection
