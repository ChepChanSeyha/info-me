@extends('Layouts.app')

@section('content')

    <style>
        .uper {
            margin-top: 40px;
        }
    </style>


    <div class="card-header">
        Edit Post
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif
        <form method="post" action="{{ route('posts.update', $post->id) }}">
            <div class="form-group">
                @csrf
                @method('PATCH')
                <label for="title">Title:</label>
                <input type="text" class="form-control" name="title" value="{{$post->title}}"/>
            </div>
            <div class="form-group">
                <label for="body">Body:</label>
                <input type="text" class="form-control" name="body" value="{{$post->body}}"/>
            </div>
            <button type="submit" class="btn btn-primary">Update Post</button>

            <a class="btn btn-primary" href="{{ route('posts.index')}}">CANCEL</a>
        </form>
    </div>

@endsection
