<!-- Bootstrap core JavaScript -->
<script src="{{ asset('theme/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('theme/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Plugin JavaScript -->
<script src="{{ asset('theme/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

<!-- Contact form JavaScript -->
<script src="{{ asset('theme/js/jqBootstrapValidation.js') }}"></script>
<script src="{{ asset('theme/js/contact_me.js') }}"></script>

<!-- Custom scripts for this template -->
<script src="{{ asset('theme/js/agency.min.js') }}"></script>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>