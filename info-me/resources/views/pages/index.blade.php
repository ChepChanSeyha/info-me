@extends('Layouts.app')

@section('content')

<div class="jumbotron text-center">
    <h1>This is index page!!!</h1>
    <h2>{{$title}}</h2>
    <p><a class="btn btn-primary btn-lg" href="/login" role="button">Login</a> <a class="btn btn-success btn-lg" href="/register" role="button">Register</a></p>

</div>

@endsection
